from rest_framework import serializers


class ProfileSerializer(serializers.Serializer):
    username = serializers.CharField()
    full_name = serializers.CharField()
    first_name = serializers.CharField()
    preferred_locale = serializers.CharField()
    email = serializers.CharField()
    role = serializers.CharField()
