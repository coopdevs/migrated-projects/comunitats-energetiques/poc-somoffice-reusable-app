from rest_framework import serializers

GENDER = [
    ('male', 'Male'),
    ('female', 'Female'),
    ('other', 'Other'),
]
LANG = [
    ('es', 'Spanish'),
    ('ca', 'Catalan'),
]
PAYMENTS = [
    ('single', 'Single'),
    ('split', 'Split'),
]
PREVIOUS_CONTRACT_TYPE = [
    ('contract', 'Contract'),
    ('prepaid', 'Prepaid'),
]
PREVIOUS_BROADBAND_SERVICE = [
    ('adsl', 'ADSL'),
    ('fiber', 'Fiber'),
]
SERVICE_TYPE = [
    ('mobile', 'mobile'),
    ('broadband', 'broadband'),
]
TYPE = [
    ('new', 'new'),
    ('portability', 'portability'),
]


class AddressSerializer(serializers.Serializer):
    street = serializers.CharField()
    city = serializers.CharField()
    zip_code = serializers.CharField()
    state = serializers.CharField()
    country = serializers.CharField()


class BroadbandISPInfoSerializer(serializers.Serializer):
    type = serializers.ChoiceField(
        choices=TYPE,
        required=True
    )
    service_address = AddressSerializer()
    previous_provider = serializers.IntegerField(required=False)
    delivery_address = AddressSerializer(required=False)
    previous_service = serializers.ChoiceField(
        choices=PREVIOUS_BROADBAND_SERVICE,
        required=False
    )
    phone_number = serializers.CharField(required=False)
    previous_owner_vat = serializers.CharField(required=False)
    previous_owner_name = serializers.CharField(required=False)
    previous_owner_lastname = serializers.CharField(required=False)
    previous_owner_surname = serializers.CharField(required=False)


class MobileISPInfoSerializer(serializers.Serializer):
    type = serializers.ChoiceField(
        choices=TYPE,
        required=True
    )
    phone_number = serializers.CharField(required=False)
    icc = serializers.CharField(required=False)
    icc_donor = serializers.CharField(required=False)
    previous_provider = serializers.IntegerField(required=False)
    previous_contract_type = serializers.ChoiceField(
        choices=PREVIOUS_CONTRACT_TYPE,
        required=False
    )
    delivery_address = AddressSerializer(allow_null=True, required=False)
    previous_owner_vat = serializers.CharField(required=False)
    previous_owner_name = serializers.CharField(required=False)
    previous_owner_lastname = serializers.CharField(required=False)
    previous_owner_surname = serializers.CharField(required=False)


class ServiceLineSerializer(serializers.Serializer):
    product_code = serializers.CharField()
    type = serializers.ChoiceField(choices=SERVICE_TYPE)
    broadband_isp_info = BroadbandISPInfoSerializer(required=False)
    mobile_isp_info = MobileISPInfoSerializer(required=False)


class ServicesFormSerializer(serializers.Serializer):
    is_company = serializers.BooleanField(required=False)
    vat = serializers.CharField(required=False)
    #TODO: rename to sponsor_vat here and odoo-python-client
    sponsor_vat = serializers.CharField(allow_null=True, required=False)
    name = serializers.CharField(required=False)
    tradename = serializers.CharField(allow_null=True, required=False)
    surname = serializers.CharField(required=False)
    lastname = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    birthdate = serializers.DateField(allow_null=True, required=False)
    gender = serializers.ChoiceField(choices=GENDER, required=False)
    phone = serializers.CharField(required=False)
    lang = serializers.ChoiceField(choices=LANG, required=False)
    nationality = serializers.CharField(required=False)
    discovery_channel_id = serializers.IntegerField(required=False)
    payment_type = serializers.ChoiceField(choices=PAYMENTS, required=False)
    iban = serializers.CharField(allow_null=True, required=False)
    service_iban = serializers.CharField(allow_null=True, required=False)
    address = AddressSerializer(required=False)
    agrees_to_voluntary_contribution = serializers.BooleanField(required=False)
    lines = ServiceLineSerializer(many=True)
