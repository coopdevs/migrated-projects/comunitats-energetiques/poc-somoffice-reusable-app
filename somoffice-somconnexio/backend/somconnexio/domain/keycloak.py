from keycloak import KeycloakAdmin
from django.conf import settings

from django.contrib.auth.backends import RemoteUserBackend
from keycloak import KeycloakOpenID, KeycloakAdmin
from keycloak.exceptions import KeycloakGetError, KeycloakAuthenticationError
from django.conf import settings
from somconnexio.domain.customer import get_customer_resource, customer_accounts_for
from somoffice.models import Profile


class KeycloakRemoteUserBackend(RemoteUserBackend):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.keycloak_admin = KeycloakAdmin(
            server_url=settings.KEYCLOAK_SERVER_URL + "/auth/",
            username=settings.KEYCLOAK_ADMIN_USER,
            password=settings.KEYCLOAK_ADMIN_PASSWORD,
            realm_name=settings.KEYCLOAK_REALM,
            verify=True,
        )

    def authenticate(self, request, username, password):
        """
        The username passed as remote_user is considered trusted. This method
        returns the user object with the given username, creating a new user
        object if create_unknown_user is True.

        Returns None if create_unknown_user is False and a User object with the
        given username is not found in the database.

        request is an HttpRequest and may be None if it wasn’t provided to
        authenticate() (which passes it on to the backend).
        """

        keycloak_openid = self._get_keycloak_open_id()

        try:
            username = username.lower()
            _token = keycloak_openid.token(username, password)

            remote_user = (self.keycloak_admin.get_users({"username": username}) or [])[
                0
            ]

            if not remote_user:
                pass

        except (KeycloakAuthenticationError, KeycloakGetError):
            return None

        user = super().authenticate(request, username)

        profile, _created = Profile.objects.get_or_create(
            remoteBackend="opencell",
            remoteId=self._get_remote_user_id(remote_user),
            user=user,
        )

        profile.save()

        return user

    def clean_username(self, username):
        """
        Performs any cleaning on the username (e.g. stripping LDAP DN
        information) prior to using it to get or create a user object. Returns
        the cleaned username.
        """

        return username

    def user_can_authenticate(self, user):
        """
        Returns whether the user is allowed to authenticate. This method
        returns False for users with is_active=False. Custom user models that
        don’t have an is_active field are allowed.
        """

        return True

    def configure_user(self, user):
        return user

    def change_password(self, username, password):
        user = (self.keycloak_admin.get_users({"username": username}) or [])[0]
        self.keycloak_admin.set_user_password(user_id=user["id"], password=password)

        # by default, users are required to update their password through keycloak, we
        # need to remove this, since we'll handle this through django
        user["requiredActions"] = []
        self.keycloak_admin.update_user(user["id"], user)

    def _get_remote_user_id(self, remote_user):
        return remote_user["attributes"]["customerCode"][0]

    def _get_customer_locale(self, remote_user_id):
        first_customer_account = list(customer_accounts_for(remote_user_id))[0]

        if first_customer_account is None:
            return None

        return self._normalize_locale(first_customer_account["language"])

    def _normalize_locale(self, locale):
        return locale.lower()[0:2]

    def get_keycloak_user(self, username):
        return (self.keycloak_admin.get_users({"username": username}) or [])[0]

    def update_user(self, keycloak_user, changes):
        self.keycloak_admin.update_user(
            keycloak_user["id"], {**keycloak_user, **changes}
        )

    def _get_keycloak_open_id(self):
        return KeycloakOpenID(
            server_url=settings.KEYCLOAK_SERVER_URL + "/auth/",
            client_id=settings.KEYCLOAK_CLIENT_ID,
            realm_name=settings.KEYCLOAK_REALM,
            client_secret_key=settings.KEYCLOAK_CLIENT_SECRET,
            verify=True,
        )

    def check_password(self, username, password):
        keycloak_openid = self._get_keycloak_open_id()

        try:
            keycloak_openid.token(username, password)
            return True
        except (KeycloakAuthenticationError, KeycloakGetError):
            return False

