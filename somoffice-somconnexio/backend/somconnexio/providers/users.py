from odoo_somconnexio_python_client.resources.partner import Partner
from odoo_somconnexio_python_client.exceptions import ResourceNotFound


class UsersProvider:
    def __init__(self, options):
        self.options = options


    def vat_exists(self, vat):
        try:
            Partner.search_by_vat(vat)
            return True
        except ResourceNotFound:
            return False

    def check_user(self, vat, id):
        try:
            partner = Partner.search_by_vat(vat)
            return str(partner.cooperator_register_number) == id
        except ResourceNotFound:
            return False
