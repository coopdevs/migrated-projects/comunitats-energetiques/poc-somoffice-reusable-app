import json
import uuid
import os

from django.conf import settings
from otrs_somconnexio.client import OTRSClient
from textwrap import dedent
from pyotrs import Ticket, Article
from pyotrs.lib import DynamicField
from somoffice.core.secure_resources import build_secure_id_generator, validate_secure_id

from somconnexio.domain.keycloak import KeycloakRemoteUserBackend
from somconnexio.serializers.tickets import TicketSerializer


class BaseTicket:
    TICKET_STATE = "new"
    TICKET_PRIORITY = "3 normal"

    def __init__(self, user, fields_dict, override_ticket_ids = []):
        self.user = user
        self.fields = fields_dict
        self.client = None
        self.override_ticket_ids = override_ticket_ids

    def _get_customer_code(self):
        raise NotImplementedError("Tickets must implement _get_customer_code")

    def _get_subject(self):
        raise NotImplementedError("Tickets must implement _get_subject")

    def _get_body(self):
        return "-"

    def _get_queue(self):
        raise NotImplementedError("Tickets must implement _get_queue")

    def _get_dynamic_fields(self):
        raise NotImplementedError("Tickets must implement _get_dynamic_fields")

    def _dict_to_dynamic_fields(self, d):
        return [DynamicField(key, d[key]) for key in d]

    def send_ticket(self):
        customer_code = self._get_customer_code()

        subject = self._get_subject()
        body = self._get_body()
        queue = self._get_queue()

        article = Article({"Subject": subject, "Body": body})
        new_ticket = Ticket({
            "Title": subject,
            "Queue": queue,
            "State": self.TICKET_STATE,
            "Type": self.TICKET_TYPE,
            "Priority": self.TICKET_PRIORITY,
            "CustomerUser": customer_code,
            "CustomerID": customer_code
        })

        dynamic_fields = self._dict_to_dynamic_fields(
            self._get_process_management_dynamic_fields()
        ) + self._dict_to_dynamic_fields(self._get_dynamic_fields())


        # TODO hack alert, ideally this process tickets should live in
        # otrs-somconnexio package. Meanwhile we need to access the raw PyOTRS client
        # to create tickets.
        self._client().client.ticket_create(
            new_ticket, article=article, dynamic_fields=dynamic_fields,
        )

        return True

    def store_ticket(self):
        ticket_path = os.path.join(settings.SOMOFFICE_TICKET_FALLBACK_PATH, str(uuid.uuid4()) + ".json")

        # ensure ticket fallback path exists
        if not os.path.exists(settings.SOMOFFICE_TICKET_FALLBACK_PATH):
            os.makedirs(settings.SOMOFFICE_TICKET_FALLBACK_PATH)

        with open(ticket_path, "w") as f:
            json.dump({ "user_id": self.user.id, "fields": self.fields }, f)

    def create(self):
        self.send_ticket()

    def _client(self):
        if self.client:
            return self.client

        self.client = OTRSClient()

        return self.client


class BaseCustomerTicket(BaseTicket):
    TICKET_TYPE = "Petición"

    def _get_customer_code(self):
        keycloak_user = KeycloakRemoteUserBackend().get_keycloak_user(self.user.username)

        if not keycloak_user:
            raise Exception(
                "Error sending email. Cannot find keycloak user for %s." % self.user.username
            )

        customer_code = self.user.profile.remoteId
        return customer_code

    def _get_process_management_dynamic_fields(self):
        result = {
            "ProcessManagementProcessID": self._get_process_id(),
            "ProcessManagementActivityID": self._get_activity_id(),
        }

        for k, v in dict(result).items():
            if v is None:
                del result[k]

        return result

    def _get_activity_id(self):
        return None

    def _get_process_id(self):
        return None


class AddDataTicket(BaseCustomerTicket):
    def _get_activity_id(self):
        return "Activity-d621f2e21d7583c0dffc7569cd3c9f59"

    def _get_process_id(self):
        return "Process-7f03577ba04a39dbc9e0fe0d2c6144df"

    def _get_subject(self):
        return "Sol·licitud abonament addicional oficina virtual"

    def _get_queue(self):
        return "Oficina Virtual::Dades addicionals::Rebut Dades"

    def _get_dynamic_fields(self):
        return {
            "liniaMobil": self.fields["phone_number"],
            "productAbonamentDadesAddicionals": self.fields["new_product_code"],
        }


class ChangeTariffTicket(BaseCustomerTicket):
    def get_search_args(self):
        return {
            "dynamic_fields": [
                DynamicField("ProcessManagementProcessID", self._get_process_id()),
                DynamicField("ProcessManagementActivityID", self._get_activity_id()),
            ],
            "Queues": [self._get_queue()],
            "States": ["new"],
        }

    def _get_activity_id(self):
        return "Activity-7117b19116339f88dc43767cd477f2be"

    def _get_process_id(self):
        return "Process-f91240baa6e0146aecc70a9c97d6f84f"

    def _get_subject(self):
        return "Sol·licitud Canvi de tarifa oficina virtual"

    def _get_queue(self):
        return "Oficina Virtual::Canvi Tarifa mòbil::Rebut"

    def _get_dynamic_fields(self):
        return {
            "renovaCanviTarifa": len(self.override_ticket_ids) > 0,
            "liniaMobil": self.fields["phone_number"],
            "productMobil": self.fields["new_product_code"],
        }

class ChangePersonalEmailTicket(BaseCustomerTicket):
    def _get_queue(self):
        return "Oficina Virtual::Canvi email compte"

    def _get_subject(self):
        return "Canvi d'email dades personals OV"

    def _get_body(self):
        email = self.fields["email"]
        return "Canviar email a %s" % email

    def _get_dynamic_fields(self):
        return {}



class BaseChangeTicket(BaseCustomerTicket):
    def _get_body(self):
        return dedent(
            f"""

            Cambiar {self._attribute_to_change()} a {self._new_value()}.

            {self._scope_of_change()}
        """
        )

    def _new_value(self):
        return self.fields["new_value"]

    def _scope_of_change(self):
        if self.fields["scope"] == "all":
            return "El cambio aplica a todos los contratos."

        output = dedent(
            f"""
            El cambio aplica a los siguientes contratos:

        """
        )

        for sub in self.fields["selected_subscriptions"]:
            output += "\n * " + sub

        return output

    def _get_attribute_dynamic_field_name(self):
        raise NotImplementedError(
            "ChangeTickets must implement _get_attribute_dynamic_field_name"
        )

    def _get_dynamic_fields(self):
        result = {
            "tipusCanviContractes": {"all": "totsContractes", "some": "selContractes"}[
                self.fields["scope"]
            ]
        }

        result[self._get_attribute_dynamic_field_name()] = self._new_value()

        return result


class ChangeEmailTicket(BaseChangeTicket):
    def _get_queue(self):
        return "Oficina Virtual::Canvi email contractes"

    def _attribute_to_change(self):
        return "email"

    def _get_subject(self):
        return "Sol·licitud canvi de email oficina virtual"

    def _get_attribute_dynamic_field_name(self):
        return "nouEmail"


class ChangeIbanTicket(BaseChangeTicket):
    def _get_queue(self):
        return "Oficina Virtual::Canvi IBAN contractes"

    def _attribute_to_change(self):
        return "IBAN"

    def _get_subject(self):
        return "Sol·licitud canvi de IBAN oficina virtual"

    def _get_attribute_dynamic_field_name(self):
        return "nouIBAN"


class ReportErrorTicket(BaseTicket):
    TICKET_TYPE = "Incidencia"

    def _get_queue(self):
        return "Sistemes::Error formularis"

    def _get_subject(self):
        return "Error submit"

    def _get_customer_code(self):
        return "sistemes@somconnexio.coop"

    def _get_body(self):
        message = f"""

            Se ha producido un error al intentar realizar el submit en los formularios con los siguientes datos:

            {json.dumps(self.fields["request"], ensure_ascii=False, indent=4)}
        """
        if self.fields["user"]:
            message = message + f"""

            Con el usuario con ref: {self.user.profile.remoteId}
            """
        return dedent(message)

    def _get_dynamic_fields(self):
        return []

    def _get_process_management_dynamic_fields(self):
        return []


def build_ticket(ticket_type, user, fields, override_ticket_ids = []):
    if ticket_type == "additional_data":
        TicketConstructor = AddDataTicket
    elif ticket_type == "change_tariff":
        TicketConstructor = ChangeTariffTicket
    elif ticket_type == "change_email":
        TicketConstructor = ChangeEmailTicket
    elif ticket_type == "change_personal_email":
        TicketConstructor = ChangePersonalEmailTicket
    elif ticket_type == "change_iban":
        TicketConstructor = ChangeIbanTicket
    elif ticket_type == "report_error":
        TicketConstructor = ReportErrorTicket
    else:
        raise ValueError("Unknown ticket type: %s" % ticket_type)

    return TicketConstructor(user, fields, override_ticket_ids)


class TicketsProvider:

    def __init__(self, options = {}):
        self.options = options
        self.secure_id_generator = build_secure_id_generator(
            user_id=options["remote_user_id"], resource_type="ticket"
        )
        self.errors = []
        self.client = OTRSClient()

    def create(self, user, meta, override_ticket_ids=[]):
        meta_dict = self._build_meta_dict(meta)

        decoded_ticket_ids = [validate_secure_id(user, id)["resource_id"] for id in override_ticket_ids]

        # find None in the list
        if None in decoded_ticket_ids:
            raise ValueError("Invalid ticket id")

        for ticket_id in decoded_ticket_ids:
            self.client.client.ticket_update(
                ticket_id,
                State="closed successful"
            )

        ticket = build_ticket(meta_dict["ticket_type"], user, meta_dict, override_ticket_ids)

        ticket.create()

        return True

    def get_resources(self, ticket_type, filters):
        user = self.options["user"]

        reference_ticket = build_ticket(ticket_type, user, {})
        search_args = reference_ticket.get_search_args()

        for filter_tuple in filters:
            name, value = filter_tuple
            search_args["dynamic_fields"].append(
                DynamicField(name, search_patterns=[value]),
            )

        tickets = self.client.search_tickets(
            **search_args,
            CustomerID=self.options["user"].profile.remoteId
        )

        tickets_dict = [
            {
                "id": self.secure_id_generator(ticket.id),
                "meta": [
                    {
                        "key": "new_product_code",
                        "value": ticket.response.dynamic_field_get("productMobil").value
                    }
                ]
            }
            for ticket in tickets
        ]

        return TicketSerializer(tickets_dict, many=True).data

    def is_valid():
        return len(self.errors) == 0

    def _build_meta_dict(self, meta):
        result = {}

        for item in meta:
            result[item["key"]] = item["value"]

        return result
