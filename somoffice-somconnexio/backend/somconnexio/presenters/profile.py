from somconnexio.domain.helpers import format_customer_name


class Profile:
    def __init__(self, current_user, customer, keycloak_user, role):
        self.current_user = current_user
        self.customer = customer
        self.keycloak_user = keycloak_user
        self.role = role

    def full_name(self):
        return format_customer_name(self.customer.name)

    def first_name(self):
        return self.customer.name["firstName"]

    def username(self):
        return self.current_user.username

    def preferred_locale(self):
        return self.current_user.profile.preferredLocale

    def email(self):
        return self.keycloak_user["email"]

    def role(self):
        return self.role
