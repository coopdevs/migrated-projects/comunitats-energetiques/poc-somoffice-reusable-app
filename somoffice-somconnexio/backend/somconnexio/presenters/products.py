from somconnexio.domain.helpers import build_custom_fields_dict


class ServiceTemplate:
    def __init__(self, serviceTemplate, language):
        self.serviceTemplate = serviceTemplate
        self.customFields = build_custom_fields_dict(
            serviceTemplate["customFields"]["customField"]
        )

        self.language = language

    def code(self):
        return self.serviceTemplate["code"]

    def tariff_selection(self):
        return self.customFields["CF_SE_TARIFF_SELECTION"]

    def description(self):
        if self.language == "ca":
            code = "CF_LANGUAGE_CAT"
        elif self.language == "es":
            code = "CF_LANGUAGE_ES"
        else:
            raise ()

        return self.customFields[code]

    def is_discontinued(self):
        result = bool(self.customFields["CF_DISCONTINUED"])

        if result:
            print("Skipping discontinued product %s" % self.code())

        return result


class OneShotChargeTemplate:
    def __init__(self, oneShotChargeTemplate, language):
        self.oneShotChargeTemplate = oneShotChargeTemplate
        self.customFields = build_custom_fields_dict(
            oneShotChargeTemplate.get("customFields", {}).get("customField", [])
        )

        self.language = language

    def code(self):
        return self.oneShotChargeTemplate["code"]

    def tariff_selection(self):
        return self.code()

    def description(self):
        return ""

    # TODO dry maybe?
    def is_discontinued(self):
        result = bool(self.customFields.get("CF_DISCONTINUED", False))

        if result:
            print("Skipping discontinued product %s" % self.code())

        return result


class Product:
    def __init__(
        self, code, description, tariff,
    ):
        self.code = code
        self.description = description
        self.tariff = tariff

    def code(self):
        return self.code

    def description(self):
        return self.description

    def price(self):
        return self.tariff["price_vat"]

    def min_included(self):
        return self.tariff.get("min_included", 0)

    def data_included(self):
        return self.tariff["data_included"]


class PriceTableBuilder:
    def __init__(self, price_table, table_name):
        self.price_table = price_table
        self.table_name = table_name

    def build(self):
        map_values = list(
            filter(lambda x: x["code"] == self.table_name, self.price_table)
        )[0]["mapValue"]

        return PriceTable(map_values)


class PriceTable:
    def __init__(self, map_values):
        self.map_values = map_values

    def is_available(self, tariff):
        return tariff in self._available_tariffs()

    def find_tariff(self, tariff):
        selected_tariff = self._tariffs()[tariff]

        values = selected_tariff["value"].split("|")
        result = {}

        for header, value in zip(self._headers(), values):
            result[header] = value

        return result

    def _available_tariffs(self):
        return [key for key in self.map_values.keys() if key != "key"]

    def _tariffs(self):
        return self.map_values

    def _headers(self):
        headers = [
            header.lower() for header in self.map_values["key"]["value"].split("/")
        ]
        # first element is the tariff code, which is not present in values to split
        headers.pop(0)
        return headers


class Products:
    def __init__(self, offer_template, language, subscription_id, charges_by_code={}):
        self.language = language
        self.subscription_id = subscription_id
        self.service_templates = offer_template["offerTemplate"]["offerServiceTemplate"]
        self.prices = offer_template["offerTemplate"]["customFields"]["customField"]
        self.charges_by_code = charges_by_code

    def change_tariff_products(self):
        price_table = self._get_price_table("change_tariff")

        service_templates = map(
            lambda st: ServiceTemplate(st["serviceTemplate"], self.language),
            self.service_templates,
        )

        filtered_service_templates = filter(
            lambda st: self._should_show_product(st, price_table), service_templates,
        )

        return [
            Product(
                code=st.code(),
                description=st.description(),
                tariff=self._tariff_for(price_table, st.tariff_selection()),
            )
            for st in filtered_service_templates
        ]

    def additional_data_products(self):
        price_table = self._get_price_table("additional_data")

        one_shot_charges = [
            OneShotChargeTemplate(oneshot["oneShotChargeTemplate"], self.language)
            for oneshot in self.charges_by_code.values()
        ]

        return [
            Product(
                code=one_shot_charge.code(),
                description=one_shot_charge.description(),
                tariff=self._tariff_for(
                    price_table, one_shot_charge.tariff_selection()
                ),
            )
            for one_shot_charge in one_shot_charges
            if self._should_show_product(one_shot_charge, price_table)
        ]

    def _should_show_product(self, product, price_table):
        return (
            price_table.is_available(product.tariff_selection())
            and not product.is_discontinued()
        )

    def _get_price_table(self, product_type):
        return PriceTableBuilder(
            self.prices, self._get_table_name_for(product_type)
        ).build()

    def _get_table_name_for(self, product_type):
        if product_type == "additional_data":
            return "CF_OF_SC_REC_TABLE_ADDICIONALS_PRICE"
        else:
            return "CF_OF_SC_REC_TABLE_PRICE"

    def _tariff_for(self, price_table, tariff_selection):
        return price_table.find_tariff(tariff_selection)
