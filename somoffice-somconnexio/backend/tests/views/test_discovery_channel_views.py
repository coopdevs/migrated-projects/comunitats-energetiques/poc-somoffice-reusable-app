import pytest
from rest_framework import status
from rest_framework.test import APITestCase


class TestDiscoveryChannel(APITestCase):
    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_discovery_channel_without_session(self):
        response = self.client.get("/api/discovery-channels/")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        channel_names = []
        for channel in response.json():
            self.assertIn("id", channel)
            self.assertIn("name", channel)

            channel_names.append(channel["name"])

        self.assertIn("Fires / Xerrades", channel_names)

    @pytest.mark.django_db
    @pytest.mark.vcr
    def test_get_discovery_channel_spanish_without_session(self):
        headers = {"Accept-Language": "es"}
        response = self.client.get("/api/discovery-channels/", headers=headers)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        channel_names = []
        for channel in response.json():
            channel_names.append(channel["name"])

        self.assertIn("Ferias / Charlas", channel_names)
