export const iconFor = type => {
  if (type === "mobile") {
    return "mobile";
  }

  if (type === "adsl" || type === "fiber") {
    return "modem";
  }

  return "unknown";
};
