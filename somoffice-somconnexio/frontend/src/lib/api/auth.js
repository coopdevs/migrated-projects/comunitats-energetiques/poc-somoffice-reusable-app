import { get, post } from "axios";
import { preprocessUsernameForApi } from '../domain/somconnexio/auth';

export const CHECK_AUTH_ENDPOINT = "/api/profile/";
export const LOGIN_ENDPOINT = '/api/auth/login/';

export const whoami = async () => {
  return (await get(CHECK_AUTH_ENDPOINT)).data;
};

export const login = async (username, password) => {
  return post("/api/auth/login/", { username: preprocessUsernameForApi(username), password });
};

export const logout = async () => {
  return post("/api/auth/logout/");
};

/**
 * Prepending ES to DNI username
 *
 * TODO: remove somconnexio specific hack
 */
export const requestPasswordReset = async ({ username }) => {
  return post("/api/auth/password_reset/", {
    email: preprocessUsernameForApi(username),
  });
};

export const validatePasswordResetToken = async ({ token }) => {
  return post("/api/auth/password_reset/validate_token/", { token });
};

export const resetPassword = async ({ token, password }) => {
  return post("/api/auth/password_reset/confirm/", { token, password });
};
