import { get, post } from "axios";

export const requestEmailChange = async ({ newEmail, currentPassword }) => {
  return await post(`/api/profile/request_email_change/`, {
    current_password: currentPassword,
    new_email: newEmail
  });
};

export const confirmEmailChange = async ({ token }) => {
  return await post(`/api/profile/confirm_email_change/`, { token });
};

export const changePassword = async ({ currentPassword, newPassword }) => {
  return await post(`/api/profile/change_password/`, {
    current_password: currentPassword,
    new_password: newPassword
  });
};

export const getProfile = async () => {
  const { data } = await get('/api/profile/')

  return data;
}
