import { get } from "axios";

export const getDiscoveryChannels = async () => {
  const { data } = await get(`/api/discovery-channels/`);

  try {
    return data;
  } catch (e) {
    console.error(e);
    /* handle error */
  }
};

