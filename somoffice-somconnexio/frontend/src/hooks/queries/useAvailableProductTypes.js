import { useTariffs } from "./useTariffs";
import { uniq } from "lodash";
import { useApplicationContext } from "hooks/useApplicationContext";

export const useAvailableProductTypes = (optingForRole = "member") => {
  const { currentUser } = useApplicationContext();
  const { isLoading, data: tariffData = [] } = useTariffs();

  const role = currentUser?.role || optingForRole || null;

  const availableProductTypes = uniq(
    tariffData
      .filter(tariff => tariff.available_for.includes(role))
      .map(tariff => tariff.category)
  );

  return {
    isLoading,
    availableProductTypes
  };
};
