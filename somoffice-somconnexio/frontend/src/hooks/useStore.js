import create from "zustand";
import { devtools } from "zustand/middleware";
import { findLastIndex, uniqueId, omit, uniqBy, compact } from "lodash";

const personalDataSteps = ["partner/personal-data", "partner/additional-data"];

const stampLineId = line => {
  if (line.__id) {
    return line;
  }

  return {
    __id: uniqueId("line"),
    ...line
  };
};

// TODO add action
//
// - removeTariffAt
// - adding/removing tariffs should affect steps
//
// TODO add derived state
//
// - canAddMobileLine
// - canAddInternetLine
export const useStore = create(
  devtools((set, get) => ({
    currentIndex: 0,
    setCurrentIndex: currentIndex => set(() => ({ currentIndex })),

    formStepDataByKey: {},
    steps: [],
    loggedIn: null,
    role: null,
    initializeSignupFlow: ({ steps: newSteps, loggedIn, role, lines }) =>
      set({
        role,
        loggedIn,
        steps: [
          ...(loggedIn ? [] : personalDataSteps),
          ...newSteps,
          ...(compact([
            mustPayMonthlyBill({ lines }) && "payment/monthly-bill",
            mustPayMemberFee({ role, loggedIn }) && "payment/member-fee",
          ]))
        ]
      }),
    setCurrentStep: step =>
      set(state => ({ currentIndex: state.steps.indexOf(step) })),
    gotoNextStep: () =>
      set(state => ({ currentIndex: state.currentIndex + 1 })),

    lines: [],
    setLines: lines => set(() => ({ lines: lines.map(stampLineId) })),

    availableAddresses: [],
    availablePaymentMethods: [],

    saveAddress: address =>
      set(state => {
        if (!address?.street) {
          return {};
        }

        const newAddress = {...address};

        if (!newAddress._id) {
          newAddress._id = uniqueId("address_");
        }

        return {
          availableAddresses: uniqBy(
            [...state.availableAddresses, newAddress],
            "_id"
          )
        };
      }),

    savePaymentMethod: paymentMethod => set(state => {
      return state
    }),

    setAvailableAddresses: availableAddresses =>
      set(() => ({ availableAddresses })),

    setAvailablePaymentMethods: availablePaymentMethods =>
      set(() => ({ availablePaymentMethods })),

    setFormStepData: (key, data) =>
      set(state => ({
        formStepDataByKey: {
          ...state.formStepDataByKey,
          [key]: data
        }
      })),

    /**
     * Internet lines comes first, so we need to add it before the first mobile
     * line, are append at the end
     *
     * TODO renumber formStepDataByKey
     */
    addInternetLine: ({ updateSteps = false } = {}) =>
      set(state => {
        const lines = state.lines;
        const newLine = stampLineId({ type: "internet" });

        const insertAfterIndex = lines.findIndex(
          line => line.type === "mobile"
        );

        if (insertAfterIndex === -1) {
          return [...lines, newLine];
        }

        const nextVal = [...lines];

        nextVal.splice(insertAfterIndex, 0, newLine);

        const result = { lines: nextVal };

        if (updateSteps) {
          const lastInternetLine =
            lines[findLastIndex(lines, line => line.type === "internet")];

          const insertAfterIndex = state.steps.indexOf(
            `line-${lastInternetLine.__id}/additional-data`
          );

          const nextSteps = [...state.steps];

          nextSteps.splice(
            insertAfterIndex + 1,
            0,
            `line-${newLine.__id}/tariff`,
            `line-${newLine.__id}/additional-data`
          );

          result.steps = nextSteps;
        }

        return result;
      }),

    /**
     * Mobile lines are always added at the end
     */
    addMobileLine: ({ updateSteps = false } = {}) =>
      set(state => {
        const newLine = stampLineId({ type: "mobile" });

        const result = {
          lines: [...state.lines, newLine]
        };

        if (updateSteps) {
          const lastMobileLine =
            state.lines[
              findLastIndex(state.lines, line => line.type === "mobile")
            ];

          const insertAfterIndex = state.steps.indexOf(
            `line-${lastMobileLine.__id}/additional-data`
          );

          const nextSteps = [...state.steps];

          nextSteps.splice(
            insertAfterIndex + 1,
            0,
            `line-${newLine.__id}/tariff`,
            `line-${newLine.__id}/additional-data`
          );

          result.steps = nextSteps;
        }

        return result;
      }),

    updateLineAt: (tariff, type, index) =>
      set(state => {
        const nextVal = [...state.lines];
        const __id = state.lines[index].__id;

        nextVal.splice(index, 1, { __id, type, ...tariff });

        return { lines: nextVal };
      }),

    removeLineAt: (index, { updateSteps = false } = {}) =>
      set(state => {
        const nextVal = [...state.lines];

        nextVal.splice(index, 1);

        const result = { lines: nextVal };

        if (updateSteps) {
          const line = state.lines[index];
          const nextSteps = [...state.steps];
          const lineIndex = state.steps.indexOf(`line-${line.__id}/tariff`);

          nextSteps.splice(lineIndex, 2);
          result.steps = nextSteps;

          const currentStep = state.steps[state.currentIndex];

          if (currentStep.startsWith(`line-${line.__id}`)) {
            const deletedLineStepIndex = state.steps.findIndex(step =>
              step.startsWith(`line-${line.__id}`)
            );
            const prevStep = state.steps[deletedLineStepIndex - 1];
            result.currentIndex = Math.max(nextSteps.indexOf(prevStep) - 1, 0);
          }
        }

        return result;
      })
  }))
);

const canAddLineOfType = state => type => {
  if (type === "internet") {
    return (
      state.role === "member" &&
      state.lines.filter(line => line.type === "internet").length < 2
    );
  } else if (type === "mobile") {
    return state.lines.filter(line => line.type === "mobile").length < 4;
  } else {
    return false;
  }
};

const mustPayMemberFee = state => !state.loggedIn && state.role === "member";
const mustPayMonthlyBill = state => (state.lines.length || []) > 0;

export const useDerivedState = () => {
  const state = useStore(state => state);

  return {
    currentStep: state.steps[state.currentIndex],
    canAddLineOfType: canAddLineOfType(state),
    shouldAskForPersonalData: !state.loggedIn,
    mustPayMemberFee: mustPayMemberFee(state),
    mustPayMonthlyBill: mustPayMonthlyBill(state),
    getAddressById: id => state.availableAddresses.find(a => a._id === id),
  };
};
