import { useStore as store } from "./useStore.js";

describe("useStore", () => {
  beforeEach(() => {
    // tell jest not to mock lodash
    jest.unmock('lodash');
    // require the actual module so that we can mock exports on the module
    const lodash = require('lodash');

    let index = 0;

    lodash.uniqueId = (prefix) => {
      return `TEST-${prefix}${++index}`
    }

    store.getState().setLines([{ type: "internet" }, { type: "mobile" }]);

    store.getState().initializeSignupFlow({
      loggedIn: false,
      role: "member",
      steps: [
        "line-TEST-line1/tariff",
        "line-TEST-line1/additional-data",
        "line-TEST-line2/tariff",
        "line-TEST-line2/additional-data"
      ]
    });
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe("addMobileLine", () => {
    describe("updateSteps = true", () => {
      it("adds mobile line at the end of list and updates steps", () => {
        store.getState().addMobileLine({ updateSteps: true });

        const state = store.getState();

        expect(state.lines).toEqual([
          { __id: 'TEST-line1', type: "internet" },
          { __id: 'TEST-line2', type: "mobile" },
          { __id: 'TEST-line3', type: "mobile" }
        ]);

        expect(state.steps).toEqual([
          "partner/personal-data",
          "partner/additional-data",
          "line-TEST-line1/tariff",
          "line-TEST-line1/additional-data",
          "line-TEST-line2/tariff",
          "line-TEST-line2/additional-data",
          "line-TEST-line3/tariff",
          "line-TEST-line3/additional-data",
          "payment/monthly-bill",
          "payment/member-fee",
        ]);
      });
    });
  });

  describe("addInternetLine", () => {
    describe("updateSteps = true", () => {
      it("adds internet line at the end after all internet lines and updates steps", () => {
        store.getState().addInternetLine({ updateSteps: true });

        const state = store.getState();

        expect(state.lines).toEqual([
          { __id: 'TEST-line1', type: "internet" },
          { __id: 'TEST-line3', type: "internet" },
          { __id: 'TEST-line2', type: "mobile" }
        ]);

        expect(state.steps).toEqual([
          "partner/personal-data",
          "partner/additional-data",
          "line-TEST-line1/tariff",
          "line-TEST-line1/additional-data",
          "line-TEST-line3/tariff",
          "line-TEST-line3/additional-data",
          "line-TEST-line2/tariff",
          "line-TEST-line2/additional-data",
          "payment/monthly-bill",
          "payment/member-fee",
        ]);
      });
    });
  });

  describe("removeLineAt", () => {
    beforeEach(() => {
      store.getState().setLines([
        { __id: "000a", type: "internet" },
        { __id: "000b", type: "internet" },
        { __id: "000c", type: "mobile" },
        { __id: "000d", type: "mobile" },
        { __id: "000e", type: "mobile" }
      ]);

      store.getState().initializeSignupFlow({
        loggedIn: false,
        role: "member",
        steps: [
          "line-000a/tariff",
          "line-000a/additional-data",
          "line-000b/tariff",
          "line-000b/additional-data",
          "line-000c/tariff",
          "line-000c/additional-data",
          "line-000d/tariff",
          "line-000d/additional-data",
          "line-000e/tariff",
          "line-000e/additional-data"
        ]
      });

      store.getState().setFormStepData("line-000d/tariff", { foo: "bar-1" });

      store
        .getState()
        .setFormStepData("line-000d/additional-data", { foo: "bar-1" });
    });

    it("removes line at specified index", () => {
      store.getState().removeLineAt(3, { updateSteps: true });

      const state = store.getState();

      expect(state.lines).toEqual([
        { __id: '000a', type: "internet" },
        { __id: '000b', type: "internet" },
        { __id: '000c', type: "mobile" },
        //{ __id: '000d', type: "mobile" },
        { __id: '000e', type: "mobile" },
      ]);

      expect(state.steps).toEqual([
        "partner/personal-data",
        "partner/additional-data",
        "line-000a/tariff",
        "line-000a/additional-data",
        "line-000b/tariff",
        "line-000b/additional-data",
        "line-000c/tariff",
        "line-000c/additional-data",
        //"line-000d/tariff",
        //"line-000d/additional-data",
        "line-000e/tariff",
        "line-000e/additional-data",
        "payment/monthly-bill",
        "payment/member-fee",
      ]);
    });
  });

  describe('when removing the currently focused line', () => {
    beforeEach(() => {
      store.getState().setLines([
        { __id: "000a", type: "internet" },
        { __id: "000b", type: "internet" },
        { __id: "000c", type: "mobile" },
        { __id: "000d", type: "mobile" },
        { __id: "000e", type: "mobile" }
      ]);

      store.getState().initializeSignupFlow({
        loggedIn: false,
        role: "member",
        steps: [
          "line-000a/tariff",
          "line-000a/additional-data",
          "line-000b/tariff",
          "line-000b/additional-data",
          "line-000c/tariff",
          "line-000c/additional-data",
          "line-000d/tariff",
          "line-000d/additional-data",
          "line-000e/tariff",
          "line-000e/additional-data"
        ]
      });

      store.getState().setFormStepData("line-000d/tariff", { foo: "bar-1" });

      store
        .getState()
        .setCurrentStep("line-000d/tariff")
    });

    it('deletes the line and focuses on previous step', () => {
      store.getState().removeLineAt(3, { updateSteps: true });

      const state = store.getState();

      expect(state.steps).toEqual([
        "partner/personal-data",
        "partner/additional-data",
        "line-000a/tariff",
        "line-000a/additional-data",
        "line-000b/tariff",
        "line-000b/additional-data",
        "line-000c/tariff",
        "line-000c/additional-data",
        //"line-000d/tariff",
        //"line-000d/additional-data",
        "line-000e/tariff",
        "line-000e/additional-data",
        "payment/monthly-bill",
        "payment/member-fee",
      ]);

      expect(state.currentIndex).toEqual(6)
    });
  })
});
