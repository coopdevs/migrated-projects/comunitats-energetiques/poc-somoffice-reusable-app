import React from "react";

export const ArrowRight = () => (
  <svg
    width="26px"
    height="15px"
    viewBox="0 0 26 15"
    fill="none"
    xmlns="http://www.w3.org/2000/svg"
  >
    <line x1="8.74228e-08" y1="7.5" x2="25" y2="7.5" stroke="currentColor" />
    <path d="M20 14L25 7.57143L20 1.14286" stroke="currentColor" />
  </svg>
);
