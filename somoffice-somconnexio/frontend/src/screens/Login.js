import React, { useState } from "react";
import Alert from "@material-ui/lab/Alert";
import { useHistory } from "react-router-dom";
import { useTranslation } from "react-i18next";

import Box from "@material-ui/core/Box";
import { FullScreenCenteredLayout } from "components/layouts/FullScreenCenteredLayout";
import { login } from "lib/api/auth";
import { Spinner } from "components/Spinner";
import { LegacyTextField } from "components/LegacyTextField";
import { Button } from "components/Button";
import { Form } from "components/LegacyForm";
import { Link } from "components/Link";
import { Stack } from "components/layouts/Stack";
import { useQueryParams } from "hooks/useQueryParams";

function LoginForm({ onLogin }) {
  const history = useHistory();
  const { redirect_to = "/home" } = useQueryParams();
  const [username, setUser] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const { t } = useTranslation();

  async function onSubmit(event) {
    try {
      setIsLoading(true);
      await login(username, password);
      await onLogin();
      history.push(redirect_to);
    } catch (e) {
      setError(t("login.errors.wrong_username_password"));
      setIsLoading(false);
    }
  }

  return (
    <Form onSubmit={onSubmit}>
      {isLoading && <Spinner />}
      {!isLoading && (
        <Stack spacing={2}>
          <LegacyTextField
            label={t("login.username")}
            value={username}
            setState={setUser}
          />
          <LegacyTextField
            type="password"
            label={t("login.password")}
            setState={setPassword}
            value={password}
          />
          <Box pt={2} width="100%">
            <Link to="/password-reset">{t("login.forgot_my_password")}</Link>
          </Box>
          {error && <Alert severity="error">{error}</Alert>}
          <Button type="submit">{t("login.submit")}</Button>
        </Stack>
      )}
    </Form>
  );
}

export const Login = ({ onLogin }) => (
  <FullScreenCenteredLayout>
    <LoginForm onLogin={onLogin} />
  </FullScreenCenteredLayout>
);
