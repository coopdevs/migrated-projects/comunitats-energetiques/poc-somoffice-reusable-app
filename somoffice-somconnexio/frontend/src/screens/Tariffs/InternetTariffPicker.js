import React, { useCallback, useEffect, useMemo, useState } from "react";
import { sortBy } from "lodash";
import { Select } from "components/Select";
import { Tiles } from "components/layouts/Tiles";
import { PricePreview } from "./PricePreview";
import { SliderSelect } from "components/SliderSelect";
import { formatBandwith } from "lib/helpers";
import Box from "@material-ui/core/Box";
import { Text } from "components/Text";
import { useTranslation } from "react-i18next";
import { Subheading } from "components/Subheading";
import { Field } from "react-final-form";
import { noop } from "lib/fn/noop";

/**
 * https://media.giphy.com/media/E87jjnSCANThe/giphy.gif
 */
const WhatDoYouWantSelect = ({ onChange, value }) => {
  const { t } = useTranslation();

  return (
    <Select
      onChange={onChange}
      value={value}
      label={t("funnel.tariffs.internet.what_do_you_want")}
      options={[
        {
          value: "fiber",
          label: t("funnel.tariffs.internet.categories.fiber")
        },
        { value: "adsl", label: t("funnel.tariffs.internet.categories.adsl") }
      ]}
    />
  );
};

const BandwidthSelector = ({ tariffs, value, onChange }) => {
  const items = sortBy(
    tariffs.map(tariff => ({
      value: Number(tariff.bandwidth),
      label: formatBandwith(tariff.bandwidth),
      code: tariff.code
    })),
    "value"
  );

  const { t } = useTranslation();

  return (
    <>
      <Box pb={1}>
        <Text size="xs">{t("funnel.tariffs.internet.what_speed")}</Text>
      </Box>
      <SliderSelect items={items} value={value} onChange={onChange} />
    </>
  );
};

export const InternetTariffPicker = ({
  tariffs,
  initialTariffCode,
  onChange = noop,
  showSubmit = false
}) => {
  const initialTariff = tariffs.find(({ code }) => code === initialTariffCode);

  const [category, setCategory] = useState(
    initialTariff ? initialTariff.category : "fiber"
  );

  const adslTariffs = tariffs.filter(({ category }) => category === "adsl");
  const fiberTariffs = tariffs.filter(({ category }) => category === "fiber");
  const categoryTariffs = category === "adsl" ? adslTariffs : fiberTariffs;

  const [tariffCode, setTariffCode] = useState(
    initialTariff?.code || categoryTariffs[0].code
  );
  const [bandwidth, setBandwidth] = useState(
    initialTariff?.bandwidth || categoryTariffs[0].bandwidth
  );

  const matchingTariff = useMemo(() => {
    let result;

    if (category === "adsl") {
      result = tariffs.find(tariff => tariff.code === tariffCode);
    } else {
      result = tariffs.find(tariff => tariff.bandwidth === bandwidth);
    }

    return result || initialTariff;
  }, [tariffs, tariffCode, bandwidth, category, initialTariff]);

  const { t } = useTranslation();

  useEffect(() => {
    if (!matchingTariff) {
      return;
    }

    onChange(matchingTariff);
  }, [matchingTariff]);

  return (
    <>
      <Box mb={4}>
        <Subheading color="text.main">
          {t("funnel.tariffs.internet.subtitle")}
        </Subheading>
      </Box>
      <Tiles spacing={4} columns={2}>
        <WhatDoYouWantSelect
          value={category}
          onChange={event => {
            const nextCategory = event.target.value;

            if (nextCategory === "adsl") {
              setTariffCode(adslTariffs[0].code);
              setBandwidth(null);
            } else {
              setTariffCode(fiberTariffs[0].code);
              setBandwidth(fiberTariffs[0].bandwidth);
            }

            setCategory(nextCategory);
          }}
        />
        <div />
      </Tiles>
      {category !== "adsl" && (
        <Box mt={4}>
          <BandwidthSelector
            value={bandwidth}
            onChange={({ value, code }) => {
              setBandwidth(value);
              setTariffCode(code);
            }}
            tariffs={fiberTariffs}
          />
        </Box>
      )}
      {category === "adsl" && adslTariffs.length > 1 && (
        <Box mt={4}>
          <Tiles spacing={4} columns={2}>
            <Select
              label="Selecciona tarifa de ADSL"
              value={initialTariffCode}
              options={adslTariffs.map(tariff => ({
                value: tariff.code,
                label: tariff.name
              }))}
              onChange={event => {
                setTariffCode(event.target.value);
              }}
            />
          </Tiles>
        </Box>
      )}
      <Box mt={4}>
        <PricePreview
          category={category}
          details={["bandwidth"]}
          tariff={matchingTariff}
          showSubmit={showSubmit}
        />
      </Box>
    </>
  );
};

InternetTariffPicker.FormField = ({ name, validate, ...props }) => (
  <Field name={name} validate={validate}>
    {({ input, meta }) => (
      <InternetTariffPicker
        {...props}
        initialTariffCode={input.value}
        onChange={({ code }) => input.onChange(code)}
      />
    )}
  </Field>
);
