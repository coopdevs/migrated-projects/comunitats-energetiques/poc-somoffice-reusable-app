import React from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Typography from "@material-ui/core/Typography";
import { useAsync } from "react-async-hook";
import { useTranslation } from "react-i18next";
import { Spinner } from "components/Spinner";
import { SidebarLayout } from "components/layouts/SidebarLayout";
import { getSubscriptionList } from "lib/api/subscriptions";
import { Switch, Route, useRouteMatch } from "react-router-dom";
import { Link } from "components/Link";
import { Stack } from "components/layouts/Stack";

import { SubscriptionDetail } from "./SubscriptionDetail";
import { SubscriptionIcon } from "./SubscriptionIcon";
import { useApplicationContext } from "hooks/useApplicationContext";

const MAX_SERVICES_TO_SHOW = 3;

const takeFirst = (n, array) => {
  const { length } = array;

  if (n >= length) {
    return [array, []];
  }

  return [array.slice(0, n), array.slice(n)];
};

const SubscriptionItem = ({ sub }) => {
  const { t } = useTranslation();

  const [services, otherServices] = takeFirst(
    MAX_SERVICES_TO_SHOW,
    sub.services
  );

  const iconSize = [40, 60]

  return (
    <Link showUnderline={false} to={`/home/subscriptions/${sub.id}`}>
      <Box component={Card} width={1} p={2}>
        <Box
          display="flex"
          alignItems="center"
          justifyContent="space-between"
          spacing={2}
        >
          <Box display="flex">
            <Box
              display="flex"
              minWidth={iconSize}
              minHeight={iconSize}
              width={iconSize}
              height={iconSize}
              mr={4}
              ml={2}
              justifyContent="center"
              alignItems="center"
            >
              <SubscriptionIcon variant="full" type={sub.subscription_type} />
            </Box>
            <Box>
              <Typography variant="h6" component="div">
                {t(`subscriptions.type.${sub.subscription_type}`)}
              </Typography>
              {services.map((service, i) => (
                <Typography
                  key={service.code}
                  display={i === services.length - 1 ? "inline" : "block"}
                >
                  {service.description}
                </Typography>
              ))}
              {otherServices.length > 0 && (
                <Typography key="other-services" variant="caption">
                  {" " +
                    t("subscriptions.list.and_other_services", {
                      count: otherServices.length
                    })}
                </Typography>
              )}
              <Typography color="textSecondary">{sub.description}</Typography>
            </Box>
          </Box>
        </Box>
      </Box>
    </Link>
  );
};

const SubscriptionList = () => {
  const { currentUser } = useApplicationContext();
  const { t } = useTranslation();

  const subscriptions = useAsync(getSubscriptionList, []);

  if (subscriptions.loading) {
    return <Spinner />;
  }

  return (
    <>
      <Box mb={2} display="flex" flexDirection={["column", "row"]} alignItems={["flex-start", "center"]} justifyContent="space-between">
        <Typography variant="h5" component="h1">
          {t("common.welcome", { name: currentUser.first_name })}
        </Typography>
        {/*<Link to="/product-picker">{t("password_reset.more_services")}</Link>*/}
      </Box>
      <Stack spacing={2}>
        {subscriptions.result.data.map(sub => (
          <Box key={sub.id} width="100%">
            <SubscriptionItem sub={sub} />
          </Box>
        ))}
      </Stack>
    </>
  );
};

export const Home = () => {
  let { path } = useRouteMatch();

  return (
    <SidebarLayout>
      <Switch>
        <Route exact path={path}>
          <SubscriptionList />
        </Route>
        <Route path={`${path}/subscriptions/:id`}>
          <SubscriptionDetail />
        </Route>
      </Switch>
    </SidebarLayout>
  );
};
