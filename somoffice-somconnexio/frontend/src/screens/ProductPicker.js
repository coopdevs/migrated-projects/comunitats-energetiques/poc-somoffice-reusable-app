import React, { useState } from "react";
import Box from "@material-ui/core/Box";
import { Card } from "components/Card";
import { Heading } from "components/Heading";
import { Subheading } from "components/Subheading";
import { Text } from "components/Text";
import { Stack } from "components/layouts/Stack";
import { NumberInput } from "components/NumberInput";
import { Columns } from "components/layouts/Columns";
import { Column } from "components/layouts/Column";
import { Button } from "components/Button";
import { IframeLayout } from "components/layouts/IframeLayout";
import { useQueryParams } from "hooks/useQueryParams";
import { Trans, useTranslation } from "react-i18next";
import { Tiles, TileSpan } from "components/layouts/Tiles";
import { Link } from "components/Link";
import { Separator } from "components/Separator";
import { Container } from "components/layouts/Container";
import { Spinner } from "components/Spinner";
import { useHistory } from "react-router-dom";
import { useAvailableProductTypes } from "hooks/queries/useAvailableProductTypes";
import { TopBarLayout } from "components/layouts/TopBarLayout";

const ActionWrapper = ({ children }) => <Box height="85px">{children}</Box>;

const ProductCard = ({
  title,
  subtitle,
  description,
  moreInfo,
  children,
  onSelect
}) => {
  return (
    <Card variant="cta" width={["100%", "auto"]} maxWidth={["auto", 350]}>
      <Box
        minHeight={238}
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="space-between"
      >
        <Box textAlign="center">
          <Box>
            <Text bold size="lg">
              {title}
            </Text>
          </Box>
          {subtitle && (
            <Box>
              <Text semibold size="sm">
                {subtitle}
              </Text>
            </Box>
          )}
        </Box>
        <Box py={2}>{children}</Box>

        <Box width={1}>
          <Stack spacing={0} align="center">
            <Button onClick={onSelect}>Seleccionar</Button>
            {false && (
              <Text color="info.dark" size="sm">
                {moreInfo}
              </Text>
            )}
          </Stack>
        </Box>
      </Box>
    </Card>
  );
};

const MobileQuantitySelector = ({ value, onChange }) => {
  const { t } = useTranslation();

  return (
    <ActionWrapper>
      <Stack align="center">
        <Text>
          {t("funnel.product_picker.product_categories.mobile.how_many")}
        </Text>
        <NumberInput onChange={onChange} value={value} min={1} max={4} />
      </Stack>
    </ActionWrapper>
  );
};

const InternetQuantitySelector = ({ value, onChange }) => {
  const [wantsMore, setWantsMore] = useState(false);
  const { t } = useTranslation();

  if (!wantsMore) {
    return (
      <ActionWrapper>
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="center"
          height="100%"
        >
          <Text
            size="sm"
            color="primary.main"
            underline
            onClick={() => setWantsMore(true)}
          >
            {t(
              "funnel.product_picker.product_categories.internet_and_phone.want_more"
            )}
          </Text>
        </Box>
      </ActionWrapper>
    );
  }

  return (
    <ActionWrapper>
      <Stack align="center">
        <Text>
          {t(
            "funnel.product_picker.product_categories.internet_and_phone.how_many"
          )}
        </Text>
        <NumberInput onChange={onChange} value={value} min={1} max={2} />
      </Stack>
    </ActionWrapper>
  );
};

export const ProductPicker = () => {
  const [mobileCount, setMobileCount] = useState(1);
  const [internetCount, setInternetCount] = useState(1);
  const [comboMobileCount, setComboMobileCount] = useState(1);
  const [comboInternetCount, setComboInternetCount] = useState(1);
  const { opting_for_role: optingForRole } = useQueryParams();
  const { t } = useTranslation();
  const { isLoading, availableProductTypes } = useAvailableProductTypes(
    optingForRole
  );
  const history = useHistory();

  const canShowInternet =
    availableProductTypes.includes("fiber") ||
    availableProductTypes.includes("adsl");

  const gotoNextStep = ({ mobileCount = 0, internetCount = 0 }) => {
    const intent = {
      optingForRole,
      lines: [
        ...Array.from({ length: internetCount }).map(() => ({
          type: "internet"
        })),
        ...Array.from({ length: mobileCount }).map(() => ({ type: "mobile" }))
      ]
    };

    const serializedIntent = JSON.stringify(intent);

    history.push(`signup/data?intent=${encodeURIComponent(serializedIntent)}`);
  };

  return (
    <IframeLayout sidebar={false}>
      <TopBarLayout>
        {isLoading ? (
          <Spinner />
        ) : (
          <Box marginTop="70px">
            <Container variant="wide">
              <Stack>
                <div>
                  <Heading>{t("funnel.product_picker.title")}</Heading>
                  <Text>
                    <Trans i18nKey="funnel.product_picker.subtitle">
                      <Link
                        openInParent
                        to={t("urls.tariffs.internet_and_phone")}
                      />
                      <Link openInParent to={t("urls.tariffs.mobile")} />
                      <Link
                        openInParent
                        to={t("urls.tariffs.mobile_internet_and_phone")}
                      />
                    </Trans>
                  </Text>
                </div>
                <Box width="100%" display="flex" justifyContent="center">
                  <Columns collapseOnSmallScreens align="center" spacing={4}>
                    <ProductCard
                      title={t(
                        "funnel.product_picker.product_categories.mobile.title"
                      )}
                      moreInfo={
                        <Trans
                          i18nKey={
                            "funnel.product_picker.product_categories.mobile.more_info"
                          }
                        >
                          <Link openInParent to={t("urls.more_info.mobile")} />
                        </Trans>
                      }
                      onSelect={() => gotoNextStep({ mobileCount })}
                    >
                      <MobileQuantitySelector
                        value={mobileCount}
                        onChange={setMobileCount}
                      />
                    </ProductCard>
                    {canShowInternet && (
                      <ProductCard
                        title={t(
                          "funnel.product_picker.product_categories.internet_and_phone.title"
                        )}
                        subtitle={t(
                          "funnel.product_picker.product_categories.internet_and_phone.subtitle"
                        )}
                        moreInfo={
                          <Trans
                            i18nKey={
                              "funnel.product_picker.product_categories.internet_and_phone.more_info"
                            }
                          >
                            <Link openInParent to={t("urls.more_info.adsl")} />
                            <Link openInParent to={t("urls.more_info.fiber")} />
                          </Trans>
                        }
                        onSelect={() => gotoNextStep({ internetCount })}
                      >
                        <InternetQuantitySelector
                          value={internetCount}
                          onChange={setInternetCount}
                        />
                      </ProductCard>
                    )}
                    {canShowInternet && (
                      <ProductCard
                        title={t(
                          "funnel.product_picker.product_categories.mobile_internet_and_phone.title"
                        )}
                        subtitle={t(
                          "funnel.product_picker.product_categories.mobile_internet_and_phone.subtitle"
                        )}
                        moreInfo={
                          <Trans
                            i18nKey={
                              "funnel.product_picker.product_categories.mobile_internet_and_phone.more_info"
                            }
                          >
                            <Link
                              openInParent
                              to={t("urls.more_info.mobile")}
                            />
                            <Link openInParent to={t("urls.more_info.adsl")} />
                            <Link openInParent to={t("urls.more_info.fiber")} />
                          </Trans>
                        }
                        onSelect={() =>
                          gotoNextStep({
                            mobileCount: comboMobileCount,
                            internetCount: comboInternetCount
                          })
                        }
                      >
                        <InternetQuantitySelector
                          value={comboInternetCount}
                          onChange={setComboInternetCount}
                        />
                        <Separator spacing={3} />
                        <MobileQuantitySelector
                          value={comboMobileCount}
                          onChange={setComboMobileCount}
                        />
                      </ProductCard>
                    )}
                  </Columns>
                </Box>
              </Stack>
              {optingForRole === "member" && (
                <Box width="100%" mt={6}>
                  <Tiles columns={9}>
                    <div />
                    <div />
                    <TileSpan span={5}>
                      <Box width="auto">
                        <Box mb={3}>
                          <Text semibold size="xl">
                            {t("funnel.product_picker.only_member.title")}
                          </Text>
                        </Box>
                        <Card variant="cta">
                          <Tiles columns={2}>
                            <Stack>
                              <Text bold>
                                {t(
                                  "funnel.product_picker.only_member.subtitle"
                                )}
                              </Text>
                              <Text>
                                {t(
                                  "funnel.product_picker.only_member.description"
                                )}
                              </Text>
                            </Stack>
                            <Button
                              onClick={() =>
                                gotoNextStep({
                                  mobileCount: 0,
                                  internetCount: 0
                                })
                              }
                            >
                              Seleccionar
                            </Button>
                          </Tiles>
                        </Card>
                      </Box>
                    </TileSpan>
                    <div />
                    <div />
                  </Tiles>
                </Box>
              )}
            </Container>
          </Box>
        )}
      </TopBarLayout>
    </IframeLayout>
  );
};
