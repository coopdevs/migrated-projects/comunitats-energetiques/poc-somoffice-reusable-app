import { getLinesFromState } from "./getLinesFromState";

const state = {
  currentIndex: 4,
  formStepDataByKey: {
    "partner/personal-data": {
      is_company: false,
      vat: "45771114x",
      name: "jorge",
      surname: "morante",
      lastname: "cabrera",
      email: "jorge@morante.eu",
      repeat_email: "jorge@morante.eu",
      birthdate: "21-12-1987",
      gender: "male",
      lang: "ca",
      nationality: "ES"
    },
    "partner/additional-data": {
      discovery_channel_id: 1
    },
    "line-0/tariff": {
      type: "mobile",
      code: "SE_SC_REC_MOBILE_T_UNL_51200"
    },
    "line-0/additional-data": {
      has_sim_card: true,
      is_prepaid: false,
      keep_number: true,
      delivery_address: {
        _id: "address_1"
      },
      previous_provider: 18,
      phone_number: "658395418",
      icc: "12341234"
    },
    "line-1/tariff": {
      type: "mobile",
      code: "SE_SC_REC_MOBILE_T_UNL_51200"
    },
    "line-1/additional-data": {
      has_sim_card: true,
      is_prepaid: false,
      keep_number: true,
      delivery_address: {
        _id: "address_1"
      },
      previous_provider: 18,
      phone_number: "658395418",
      icc: "12341234"
    }
  },
  steps: [
    "partner/personal-data",
    "partner/additional-data",
    "line-0/tariff",
    "line-0/additional-data",
    "payment/monthly-bill",
    "payment/member-fee",
  ],
  lines: [
    {
      __id: '0',
      type: "mobile"
    },
    {
      __id: '1',
      type: "mobile"
    },
  ],
  availableAddresses: [
    {
      _id: "address_1"
    }
  ]
};

describe("getLinesFromState", () => {
  it("returns and array of lines from complete state blob", () => {
    expect(getLinesFromState(state)).toEqual([
      {
        type: "mobile",
        code: "SE_SC_REC_MOBILE_T_UNL_51200",
        has_sim_card: true,
        is_prepaid: false,
        keep_number: true,
        delivery_address: {
          _id: "address_1"
        },
        previous_provider: 18,
        phone_number: "658395418",
        icc: "12341234"
      },
      {
        type: "mobile",
        code: "SE_SC_REC_MOBILE_T_UNL_51200",
        has_sim_card: true,
        is_prepaid: false,
        keep_number: true,
        delivery_address: {
          _id: "address_1"
        },
        previous_provider: 18,
        phone_number: "658395418",
        icc: "12341234"
      }
    ]);
  });
});
