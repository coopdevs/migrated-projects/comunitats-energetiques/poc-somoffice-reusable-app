import React, { useState, useRef } from "react";
import { Stack } from "components/layouts/Stack";
import { Inline } from "components/layouts/Inline";
import { Button } from "components/Button";
import uniq from "lodash.uniq";
import compact from "lodash.compact";
import Box from "@material-ui/core/Box";
import { LegacyTextField } from "components/LegacyTextField";
import ibanLib from "iban";
import { useRequestChangeContext } from "../context";
import { useTranslation } from "react-i18next";
import Radio from "@material-ui/core/Radio";
import Alert from "@material-ui/lab/Alert";
import Checkbox from "@material-ui/core/Checkbox";
import { formatIBAN } from 'lib/formatIBAN';
import FormControlLabel from '@material-ui/core/FormControlLabel';

export const ChangeIban = () => {
  const { t } = useTranslation();

  const {
    setNewValue,
    subscriptions,
    requestConfirm
  } = useRequestChangeContext();

  const [isConfirmed, setIsConfirmed] = useState(false);
  const [selectedIBAN, setSelectedIBAN] = useState(null);
  const [newIBAN, setNewIBAN] = useState(null);
  const ibans = compact(uniq(subscriptions.map(({ iban }) => iban)));
  const [hasInvalidIBAN, setHasInvalidIBAN] = useState(false);
  const textInputRef = useRef(null);

  const clearErrors = () => {
    setTimeout(() => {
      setHasInvalidIBAN(false);
    }, 250);
  };

  const onClick = () => {
    if (selectedIBAN === "new-iban" && !ibanLib.isValid(newIBAN)) {
      setHasInvalidIBAN(true);
      return;
    }

    setNewValue(selectedIBAN === "new-iban" ? newIBAN : selectedIBAN);
    requestConfirm();
  };

  return (
    <div>
      <strong>{t("request_change.change_iban.title")}</strong>
      <Stack>
        {ibans.map(iban => (
          <Box
            display="flex"
            alignItems="center"
            fontSize={[12, 14]}
            cursor="pointer"
            key={iban}
            onClick={() => setSelectedIBAN(iban)}
          >
            <Radio
              color="primary"
              value={iban}
              checked={iban === selectedIBAN}
            />
            {formatIBAN(iban)}
          </Box>
        ))}
        <Box
          display="flex"
          flexDirection="row"
          onClick={() => setSelectedIBAN("new-iban")}
        >
          <Radio
            color="primary"
            value="new-iban"
            checked={selectedIBAN === "new-iban"}
          />
          <LegacyTextField
            ref={textInputRef}
            variant="standard"
            fullWidth={false}
            setState={setNewIBAN}
            onFocus={clearErrors}
            label={t("request_change.change_iban.add_new_iban")}
          />
        </Box>
        {hasInvalidIBAN && (
          <Box my={1}>
            <Alert severity="error">
              {t("request_change.change_iban.invalid_iban")}
            </Alert>
          </Box>
        )}
        <div style={{cursor: 'pointer'}}>
          <Box mt={4} display="flex" onClick={() => setIsConfirmed(val => !val)}>
            <Checkbox checked={isConfirmed} color="primary" />
            <Box display="flex" alignItems="center" fontSize="0.75rem">
              {t('request_change.change_iban.confirm_checkbox')}
            </Box>
          </Box>
        </div>
        <Box mt={2}>
          <Button fullWidth={false} onClick={onClick} disabled={!(isConfirmed && Boolean(selectedIBAN || newIBAN))}>
            {t("common.confirm")}
          </Button>
        </Box>
      </Stack>
    </div>
  );
};
