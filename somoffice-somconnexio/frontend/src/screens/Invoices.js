import React, { useState, Fragment } from "react";
import Card from "@material-ui/core/Card";
import Box from "@material-ui/core/Box";
import Alert from "@material-ui/lab/Alert";
import { Button } from "components/Button";
import Typography from "@material-ui/core/Typography";
import { useTranslation } from "react-i18next";
import { makeStyles } from "@material-ui/core/styles";
import { Stack } from "components/layouts/Stack";

import { SidebarLayout } from "components/layouts/SidebarLayout";
import { useAsync } from "react-async-hook";
import { getInvoicesList, checkInvoice } from "lib/api/invoices";
import { toDate, format } from "date-fns";
import { ca, es } from "date-fns/locale";
import { useApplicationContext } from "hooks/useApplicationContext";
import { Spinner } from "components/Spinner";
import { capitalize } from "lib/string/capitalize";
import { byDate } from "lib/comparators/byDate";

const getDateLocale = locale => {
  return {
    es,
    ca
  }[locale];
};

const useStyles = makeStyles(theme => ({
  amount: {
    fontSize: theme.typography.h5.fontSize,
    display: "flex",
    justifyContent: "flex-end"
  },
  date: {
    [theme.breakpoints.down("sm")]: {
      fontSize: theme.typography.h6.fontSize,
    },
    display: "flex",
    fontSize: theme.typography.h5.fontSize,
  },
  number: {
    paddingLeft: theme.spacing(1),
    color: theme.palette.grey["600"]
  }
}));

const Invoice = ({ invoice }) => {
  const [isLoading, setIsLoading] = useState(false);
  const [hasErrors, setHasErrors] = useState(false);
  const { currentUser } = useApplicationContext();
  const { t } = useTranslation();
  const classes = useStyles();

  const onClickDownload = async event => {
    setIsLoading(true);
    event.preventDefault();

    const isInvoiceAvailable = await checkInvoice({ id: invoice.id });

    if (isInvoiceAvailable) {
      window.location = invoice.download_url;
    } else {
      setHasErrors(true);
    }

    setIsLoading(false);
  };

  return (
    <Box component={Card} display="flex" flex={1} p={2} width="100%">
      <Stack fullWidth>
        <Box pl={1} display="flex" width="100%" alignItems="baseline" justifyContent="space-between">
          <div className={classes.date}>
            {capitalize(
              format(toDate(invoice.date), "MMMM yyyy", {
                locale: getDateLocale(currentUser.preferred_locale)
              })
            )}
          </div>
          <div className={classes.amount}>
            {invoice.total_amount.toLocaleString(currentUser.preferred_locale, {
              minimumFractionDigits: 2,
              maximumFractionDigits: 2
            })}
            €
          </div>
        </Box>
        <div className={classes.number}>{invoice.number}</div>
        <Box paddingTop={2}>
          {!hasErrors && (
            <Button
              variant="text"
              onClick={onClickDownload}
              href={invoice.download_url}
              color="primary"
              disabled={isLoading}
              fullWidth={false}
            >
              {isLoading ? t("common.loading") : t("common.download")}
            </Button>
          )}

          {hasErrors && (
            <Alert severity="error">{t("invoices.download_error")}</Alert>
          )}
        </Box>
      </Stack>
    </Box>
  );
};

const InvoiceList = ({ invoices }) => {
  const { t } = useTranslation();

  return (
    <Fragment>
      <Box mb={2}>
        <Typography variant="h5" component="h1">
          {t("invoices.description")}
        </Typography>
      </Box>
      <Stack fullWidth spacing={2}>
        {invoices.sort(byDate).map(invoice => (
          <Invoice invoice={invoice} />
        ))}
      </Stack>
    </Fragment>
  );
};

export const Invoices = () => {
  const invoices = useAsync(getInvoicesList, []);

  return (
    <SidebarLayout>
      {invoices.loading && <Spinner />}
      {invoices.result && <InvoiceList invoices={invoices.result.data} />}
    </SidebarLayout>
  );
};
