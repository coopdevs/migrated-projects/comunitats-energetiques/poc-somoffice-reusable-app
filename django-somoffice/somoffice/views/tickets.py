from rest_framework import status
from rest_framework.views import APIView
from rest_framework.viewsets import ViewSet
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from somoffice.core.errors import ProviderError
from somoffice.core.http import with_secure_token
from somoffice.core import load_resource_provider


class TicketsViewSet(ViewSet):
    authentication_classes = [SessionAuthentication]
    permission_classes = [IsAuthenticated]

    def list(self, request):
        options = {
            "remote_user_id": request.user.profile.remoteId,
            "user": request.user,
        }
        ticket_type = request.query_params.get("ticket_type")

        # TODO ideally we should use the same meta interface as in creation
        filters = [(param, request.query_params.get(param)) for param in request.query_params.keys() if not param == "ticket_type"]
        provider = load_resource_provider("tickets")(options)
        return Response(provider.get_resources(ticket_type, filters))


    def post(self, request):
        options = {
            "remote_user_id": request.user.profile.remoteId,
            "user": request.user,
        }
        provider = load_resource_provider("tickets")(options)

        try:
            provider.create(
                user=request.user,
                meta=request.data["meta"],
                override_ticket_ids=request.data.get("override_ticket_ids", []),
            )

            return Response({"msg": "ok"})
        except ProviderError as e:
            return Response({"msg": str(e)}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
        except Exception as e:
            print("Error creating ticket")
            print(repr(e))
            return Response({"msg": "error"}, status=status.HTTP_400_BAD_REQUEST)
