from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response

from somoffice.core import load_resource_provider


class ProductCatalog(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request):

        code = request.query_params.get("code")
        category = request.query_params.get("categ")
        if not code:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            options = {"code": code, "category": category}
            provider = load_resource_provider("product_catalog")(options)

            return Response(provider.get_resources())
