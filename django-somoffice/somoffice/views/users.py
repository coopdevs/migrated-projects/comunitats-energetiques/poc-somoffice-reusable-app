from rest_framework import status
from rest_framework.response import Response
from somoffice.core import load_resource_provider
from rest_framework.decorators import api_view


@api_view(("GET",))
def search_by_vat(request):
    vat = request.query_params.get("vat")
    options = {}
    if not vat:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    else:
        provider = load_resource_provider("users")(options)

        if not provider.vat_exists(vat):
            return Response(status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(status=status.HTTP_200_OK)

@api_view(("GET",))
def check_user(request):
    vat = request.query_params.get("vat")
    id = request.query_params.get("id")
    options = {}
    provider = load_resource_provider("users")(options)

    if provider.check_user(vat, id):
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)
